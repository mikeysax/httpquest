# httpquest

A wrapper to simplify the fetch API.

## Installation

> npm install httpquest

## Reference

 * [httpRequest](#httprequest)
 * [get](#get)
 * [post](#post)
 * [put](#put)
 * [patch](#patch)
 * [delete](#delete)

### httpRequest

Syntax:
```js
import httpRequest from 'httpquest';

httpRequest(url, method, body, options)
.then(json => console.log(json))
.catch(error => console.error(error));
```

#### url
Type: String 

A string representing the URL of the request.

#### method
Type: String

A case-insensitive string representing the method of the request.

- `'HEAD'`
- `'GET'`
- `'POST'`
- `'PUT'`
- `'PATCH'`
- `'DELETE'`

#### body (depending on method, optional)
Type: Object

An object of either FormData or JSON required for **only** `POST`, `PUT` and `PATCH` requests which contain the payload data.

#### options (optional)
Type: Object

An object that contains other paremeters that the Fetch API accepts.

Example:
```js
{
  credentials: 'include',
  headers: { xsrfToken: token },
  params: { search: 'dog' }
}
```

### get
Syntax:
```js
import { get } from 'httpquest';

get(url, options)
.then(json => console.log(json))
.catch(error => console.error(error));
```

#### url
Type: String 

A string representing the URL of the request.

#### options (optional)
Type: Object

An object that contains other paremeters that the Fetch API accepts.

Example:
```js
{
  credentials: 'include',
  headers: { xsrfToken: token },
  params: { search: 'dog' }
}
```

### post
Syntax:
```js
import { post } from 'httpquest';

post(url, body, options)
.then(json => console.log(json))
.catch(error => console.error(error));
```

#### url
Type: String 

A string representing the URL of the request.

#### body (depending on method, optional)
Type: Object

An object of either FormData or JSON required for **only** `POST`, `PUT` and `PATCH` requests which contain the payload data.

#### options (optional)
Type: Object

An object that contains other paremeters that the Fetch API accepts.

Example:
```js
{
  credentials: 'include',
  headers: { xsrfToken: token },
  params: { search: 'dog' }
}
```

### put
Syntax:
```js
import { put } from 'httpquest';

put(url, body, options)
.then(json => console.log(json))
.catch(error => console.error(error));
```

#### url
Type: String 

A string representing the URL of the request.

#### body (depending on method, optional)
Type: Object

An object of either FormData or JSON required for **only** `POST`, `PUT` and `PATCH` requests which contain the payload data.

#### options (optional)
Type: Object

An object that contains other paremeters that the Fetch API accepts.

Example:
```js
{
  credentials: 'include',
  headers: { xsrfToken: token },
  params: { search: 'dog' }
}
```

### patch
Syntax:
```js
import { patch } from 'httpquest';

patch(url, body, options)
.then(json => console.log(json))
.catch(error => console.error(error));
```

#### url
Type: String 

A string representing the URL of the request.

#### body (depending on method, optional)
Type: Object

An object of either FormData or JSON required for **only** `POST`, `PUT` and `PATCH` requests which contain the payload data.

#### options (optional)
Type: Object

An object that contains other paremeters that the Fetch API accepts.

Example:
```js
{
  credentials: 'include',
  headers: { xsrfToken: token },
  params: { search: 'dog' }
}
```

### delete
Syntax:
```js
import { delete } from 'httpquest';

delete(url, options)
.then(json => console.log(json))
.catch(error => console.error(error));
```

#### url
Type: String 

A string representing the URL of the request.

#### body (depending on method, optional)
Type: Object

An object of either FormData or JSON required for **only** `POST`, `PUT` and `PATCH` requests which contain the payload data.

#### options (optional)
Type: Object

An object that contains other paremeters that the Fetch API accepts.

Example:
```js
{
  credentials: 'include',
  headers: { xsrfToken: token },
  params: { search: 'dog' }
}
```

<!-- ## System Requirements -->
<!-- 
 * [node.js](http://nodejs.org/) v0.8.5+. Previous versions do not have proper SSL support.
 * [form-data](https://github.com/felixge/node-form-data)
 * [mmmagic](https://github.com/mscdex/mmmagic) -->
