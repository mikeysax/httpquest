require('whatwg-fetch');
require('promise-polyfill');

const httpRequest = (url, method, body = {}, options = {}) => {
  // Type Check
  if (typeof url !== 'string') throw new TypeError('httpRequest: URL must be a string.');
  if (typeof method !== 'string') throw new TypeError('httpRequest: Method must be a string.');
  if (typeof body !== 'object') throw new TypeError('httpRequest: Body must be an object.');
  if (typeof options !== 'object') throw new TypeError('httpRequest: Options must be an object.');
  if (!method.match(/((post)|(patch)|(put)|(get)|(delete)|(head))/i)) return console.error(`httpRequest: method ${method} is not a valid HTTP request.`);
  if (options.params && typeof options.params !== 'object') throw new TypeError('httpRequest: option params must be an object.');
  if (options.headers && typeof options.headers !== 'object') throw new TypeError('httpRequest: option headers must be an object.');

  // Determines the base fetch data.
  let fetchObj = { method: method, headers: {} };

  // Determine if Body is an Object or FormData
  if (method.match(/((post)|(patch)|(put))/i)) {
    if (body.constructor.name === 'Object') {
      fetchObj.body = JSON.stringify(body);
      fetchObj.headers['Accept'] = 'application/json';
      fetchObj.headers['Content-Type'] = 'application/json';
    } else {
      fetchObj.body = body;
    }
  }

  // Determine if there are other headers
  if (options.headers) {
    Object.entries(options.headers).forEach((h) => {
      if (options.headers[h[0]]) fetchObj.headers[h[0]] = h[1];
    });
    delete options.headers;
  }

  // Determine if there are query params in options. Add { params: { token: 'afwaf' }} to options to add query string params.
  if (options.params) {
    let queryParams = [];
    for (let p in options.params) {
      if (options.params[p]) queryParams.push(encodeURIComponent(p) + '=' + encodeURIComponent(options.params[p]));
    }
    url = `${url}?${queryParams.join('&')}`;
    delete options.params;
  }

  // This makes httpRequest flexible...Add anything extra that httpRequest doesn't automatically.
  if (options) fetchObj = Object.assign({}, fetchObj, options);

  // The HTTP Request
  return fetch(url, fetchObj)
    .then((response) => {
      const contentType = response.headers.get('content-type');
      if (contentType) {
        if (contentType.match('json')) {
          // Response is 2** Status, Grab json and return promise.
          if (response.ok) return response.json();
          // Triggers catch callback if there is an issue.
          return response.json().then(err => { throw err; });
        } else if (contentType.match('form')) {
          // Response is 2** Status, Grab form data and return promise.
          if (response.ok) return response.formData();
          // Triggers catch callback if there is an issue.
          return response.formData().then(err => { throw err; });
        } else if (contentType.match('text')) {
          // Response is 2** Status, Grab text data and return promise.
          if (response.ok) return response.text();
          // Triggers catch callback if there is an issue.
          return response.text().then(err => { throw err; });
        }
      }
      return response;
    })

  // When calling this method, it will return a promise which requires another then and catch callback.
};

export const get = (url, options = {}) => {
  return httpRequest(url, 'GET', {}, options);
};

export const post = (url, body = {}, options = {}) => {
  return httpRequest(url, 'POST', body, options);
};

export const put = (url, body = {}, options = {}) => {
  return httpRequest(url, 'PUT', body, options);
};

export const patch = (url, body = {}, options = {}) => {
  return httpRequest(url, 'PATCH', body, options);
};

export const del = (url, options = {}) => {
  return httpRequest(url, 'DELETE', {}, options);
};

export default httpRequest;